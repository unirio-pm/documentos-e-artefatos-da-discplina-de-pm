# Documentos e artefatos da discplina de PM

Plano de aula e artefatos utilizados durante a disciplina de Programação Modular da UNIRIO. Repositório disponibilizado para a publicação na trilha de educação do Simpósio Brasileiro de Engenharia de Software. 

Professores que desejem acessar artefatos de base para avaliação dos trabalhos: modelagem Swagger, código do projeto etc devem solicitar de forma privada em pasemes@uniriotec.br.
